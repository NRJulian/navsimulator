/*
 * Copyright (C) 2020 Nick Julian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.navsimulator.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.text.DecimalFormat;

/**
 *
 * @author Nick Julian
 */
public class Position {

    //Values for decimal
    private double decimalLatitude;
    private double decimalLongitude;
    //Values for DMS
    private int latitudeDegrees;
    private int latitudeMinutes;
    private double latitudeSeconds;
    private int longitudeDegrees;
    private int longitudeMinutes;
    private double longitudeSeconds;
    //Values for GPS
    //private double gpsLatitude;
    //private double gpsLongitude;
    private String longitudeDirectionIndicator;
    private String latitudeDirectionIndicator;

    private DecimalFormat gpsDecimalFormat = new DecimalFormat("##.##");
    private DecimalFormat secondsDecimalFormat = new DecimalFormat("##.####");
    private DecimalFormat precisionDecimalFormat = new DecimalFormat("#.############");

    @JsonCreator
    public Position(@JsonProperty("decimalLatitude") double decimalLatitude, @JsonProperty("decimalLongitude") double decimalLongitude) {
        this.decimalLatitude = Double.valueOf(precisionDecimalFormat.format(decimalLatitude));
        this.decimalLongitude = Double.valueOf(precisionDecimalFormat.format(decimalLongitude));
        this.latitudeDegrees = extractDegrees(decimalLatitude);
        this.latitudeMinutes = extractMinutes(decimalLatitude);
        this.latitudeSeconds = Double.valueOf(secondsDecimalFormat.format(extractSeconds(decimalLatitude)));
        if (latitudeMinutes >= 60) {
            latitudeMinutes = 0;
            latitudeDegrees++;
        }
        if (latitudeSeconds >= 60) {
            latitudeSeconds = 0;
            latitudeMinutes++;
        }
        if (decimalLatitude > 0) {
            this.latitudeDirectionIndicator = "N";
        } else {
            this.latitudeDirectionIndicator = "S";
        }
        this.longitudeDegrees = extractDegrees(decimalLongitude);
        this.longitudeMinutes = extractMinutes(decimalLongitude);
        this.longitudeSeconds = Double.valueOf(secondsDecimalFormat.format(extractSeconds(decimalLongitude)));
        if (longitudeMinutes >= 60) {
            longitudeMinutes = 0;
            longitudeDegrees++;
        }
        if (longitudeSeconds >= 60) {
            longitudeSeconds = 0;
            longitudeMinutes++;
        }
        if (decimalLongitude > 0) {
            this.longitudeDirectionIndicator = "E";
        } else {
            this.longitudeDirectionIndicator = "W";
        }
    }

    public double getDecimalLatitude() {
        return decimalLatitude;
    }

    public void setDecimalLatitude(double decimalLatitude) {
        this.decimalLatitude = decimalLatitude;
    }

    public double getDecimalLongitude() {
        return decimalLongitude;
    }

    public void setDecimalLongitude(double decimalLongitude) {
        this.decimalLongitude = decimalLongitude;
    }

    //String methods
    
    public String getDMSPositionString() {
      return getDMSLatitudeString() + ", " + getDMSLongitudeString();
    }
    
    public String getDMSLatitudeString() {
        return latitudeDegrees + "°" + latitudeMinutes + "'" + latitudeSeconds + "''" + latitudeDirectionIndicator;
    }

    public String getDMSLongitudeString() {
        return longitudeDegrees + "°" + longitudeMinutes + "'" + longitudeSeconds + "''" + longitudeDirectionIndicator;
    }
    
    public String getGPSPositionString() {
      return getGPSLatitudeString() + ", " + getGPSLongitudeString();
    }
    
    public String getGPSLatitudeString() {
        double latitudeMinutesAndSecondsAsDecimal = (double) latitudeMinutes + ((double) latitudeSeconds / 60);
        return latitudeDegrees + "°" + gpsDecimalFormat.format(latitudeMinutesAndSecondsAsDecimal) + latitudeDirectionIndicator;
    }

    public String getGPSLongitudeString() {
        double longitudeMinutesAndSecondsAsDecimal = (double) longitudeMinutes + ((double) longitudeSeconds / 60);
        return longitudeDegrees + "°" + gpsDecimalFormat.format(longitudeMinutesAndSecondsAsDecimal) + longitudeDirectionIndicator;
    }

    public String getLatitudeDegreesString() {
        return String.valueOf(latitudeDegrees);
    }

    public String getLatitudeMinutesString() {
        return String.valueOf(latitudeMinutes);
    }

    public String getLatitudeSecondsString() {
        return String.valueOf(latitudeSeconds);
    }

    public String getLongitudeDegreesString() {
        return String.valueOf(longitudeDegrees);
    }

    public String getLongitudeMinutesString() {
        return String.valueOf(longitudeMinutes);
    }

    public String getLongitudeSecondsString() {
        return String.valueOf(longitudeSeconds);
    } 
    
    //Math methods
    //These 3 methods adapted from https://adamprescott.net/2013/07/17/convert-latitudelongitude-between-decimal-and-degreesminutesseconds-in-c/
    public static int extractDegrees(double value) {
        return Math.abs((int) value);
    }

    public static int extractMinutes(double value) {
        value = Math.abs(value);
        return (int) ((value - extractDegrees(value)) * 60);
    }

    public static int extractSeconds(double value) {
        value = Math.abs(value);
        double minutes = (value - extractDegrees(value)) * 60;
        return (int) Math.round((minutes - extractMinutes(value)) * 60);
    }

}
