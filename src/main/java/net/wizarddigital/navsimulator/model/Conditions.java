/*
 * Copyright (C) 2020 Nick Julian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.navsimulator.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import net.wizarddigital.navsimulator.util.MathUtil;

/**
 *
 * @author Nick Julian
 */
public class Conditions {

    private double set; //Set (current) in degrees
    private double drift; //Drift (current speed) in konts
    private double windDirection; //Wind direction in degrees
    private double windSpeed; //Wind speed in knots
    private double leeway; //Leeway from wind in degrees 

    @JsonCreator
    public Conditions(@JsonProperty("set") double set, @JsonProperty("drift") double drift, @JsonProperty("windDirection") double windDirection, @JsonProperty("leeway") double leeway) {
        setSet(set);
        setDrift(drift);
        setWindDirection(windDirection);
        setLeeway(leeway);
    }

    public double getSet() {
        return set;
    }

    public void setSet(double set) {
        this.set = MathUtil.normalizeCompassHeading(set);
    }

    public double getDrift() {
        return drift;
    }

    public void setDrift(double drift) {
        drift = MathUtil.ensureValueIsInRange(drift, 0, 10);
        this.drift = drift;
    }

    public double getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(double windDirection) {
        this.windDirection = MathUtil.normalizeCompassHeading(windDirection);
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        windSpeed = MathUtil.ensureValueIsInRange(windSpeed, 0, 20);
        this.windSpeed = windSpeed;
    }

    public double getLeeway() {
        return leeway;
    }

    public void setLeeway(double leeway) {
        leeway = MathUtil.ensureValueIsInRange(leeway, 0, 25);
        this.leeway = leeway;
    }

    public void randomlyAlterConditions(Options options, boolean alterSet, boolean alterDrift, boolean alterWind, boolean alterWindSpeed, boolean alterLeeway) {
        if (alterSet) {
            setSet(set + MathUtil.getValueAlteration(options.getSetAlterationChance(), options.getMaxSetChangeInterval()));
        }
        if (alterDrift) {
            setDrift(drift + MathUtil.getValueAlteration(options.getDriftAlterationChance(), options.getMaxDriftChangeInterval()));
        }
        if (alterWind) {
            setWindDirection(windDirection + MathUtil.getValueAlteration(options.getWindDirectionAlterationChance(), options.getMaxWindDirectionChangeInterval()));
        }
        if (alterWindSpeed) {
            setWindSpeed(windSpeed + MathUtil.getValueAlteration(options.getWindSpeedAlterationChance(), options.getMaxWindSpeedChangeInterval()));
        }
        if (alterLeeway) {
            setLeeway(leeway + MathUtil.getValueAlteration(options.getLeewayAlterationChance(), options.getMaxLeewayChangeInterval()));
        }
    }
    
    public double calculateLeeway() {
        if (windSpeed <= 5) {
          return 0;
        } else if (windSpeed > 5 && windSpeed <= 10) {
          return 1;
        } else if (windSpeed > 10 && windSpeed <= 15) {
          return 3;
        }
        return 5;
    }
    
    public String getWindReport() {
      return new String ("Wind from " + windDirection + "° at " + windSpeed + " knots.");
    }
}
