/*
 * Copyright (C) 2020 Nick Julian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.navsimulator.model;

/**
 *
 * @author Nick Julian
 */
public class Options {
    private double maxDriftChangeInterval, //In knots per hour
      maxLeewayChangeInterval, //In degrees per hour
      maxSetChangeInterval, //In degrees per hour
      maxWindDirectionChangeInterval, //In degrees per hour
      maxWindSpeedChangeInterval; //in knots
    
    private double driftAlterationChance, leewayAlterationChance, setAlterationChance, windDirectionAlterationChance, windSpeedAlterationChance; //All expressed as decimal with 1 being 100%
    private boolean sailingMode = false; //In sailing mode, your speed is determined by the wind speed and direction relative to the boat
    private boolean linkLeewayToWindSpeed = true;
    private boolean keepLog = true;

    public double getMaxDriftChangeInterval() {
        return maxDriftChangeInterval;
    }

    public void setMaxDriftChangeInterval(double maxDriftChangeInterval) {
        this.maxDriftChangeInterval = maxDriftChangeInterval;
    }

    public double getMaxLeewayChangeInterval() {
        return maxLeewayChangeInterval;
    }

    public void setMaxLeewayChangeInterval(double maxLeewayChangeInterval) {
        this.maxLeewayChangeInterval = maxLeewayChangeInterval;
    }

    public double getMaxSetChangeInterval() {
        return maxSetChangeInterval;
    }

    public void setMaxSetChangeInterval(double maxSetChangeInterval) {
        this.maxSetChangeInterval = maxSetChangeInterval;
    }

    public double getMaxWindDirectionChangeInterval() {
        return maxWindDirectionChangeInterval;
    }

    public void setMaxWindDirectionChangeInterval(double maxWindDirectionChangeInterval) {
        this.maxWindDirectionChangeInterval = maxWindDirectionChangeInterval;
    }

    public double getMaxWindSpeedChangeInterval() {
        return maxWindSpeedChangeInterval;
    }

    public void setMaxWindSpeedChangeInterval(double maxWindSpeedChangeInterval) {
        this.maxWindSpeedChangeInterval = maxWindSpeedChangeInterval;
    }

    public double getDriftAlterationChance() {
        return driftAlterationChance;
    }

    public void setDriftAlterationChance(double driftAlterationChance) {
        this.driftAlterationChance = driftAlterationChance;
    }

    public double getLeewayAlterationChance() {
        return leewayAlterationChance;
    }

    public void setLeewayAlterationChance(double leewayAlterationChance) {
        this.leewayAlterationChance = leewayAlterationChance;
    }

    public double getSetAlterationChance() {
        return setAlterationChance;
    }

    public void setSetAlterationChance(double setAlterationChance) {
        this.setAlterationChance = setAlterationChance;
    }

    public double getWindDirectionAlterationChance() {
        return windDirectionAlterationChance;
    }

    public void setWindDirectionAlterationChance(double windDirectionAlterationChance) {
        this.windDirectionAlterationChance = windDirectionAlterationChance;
    }

    public double getWindSpeedAlterationChance() {
        return windSpeedAlterationChance;
    }

    public void setWindSpeedAlterationChance(double windSpeedAlterationChance) {
        this.windSpeedAlterationChance = windSpeedAlterationChance;
    }

    public boolean isSailingMode() {
        return sailingMode;
    }

    public void setSailingMode(boolean sailingMode) {
        this.sailingMode = sailingMode;
    }

    public boolean isLinkLeewayToWindSpeed() {
        return linkLeewayToWindSpeed;
    }

    public void setLinkLeewayToWindSpeed(boolean linkLeewayToWindSpeed) {
        this.linkLeewayToWindSpeed = linkLeewayToWindSpeed;
    }

    public boolean isKeepLog() {
        return keepLog;
    }

    public void setKeepLog(boolean keepLog) {
        this.keepLog = keepLog;
    }
    
    
}
