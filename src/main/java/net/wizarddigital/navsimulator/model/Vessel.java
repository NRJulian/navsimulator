/*
 * Copyright (C) 2020 Nick Julian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.navsimulator.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.awt.geom.Point2D;
import net.wizarddigital.navsimulator.util.MathUtil;
import org.geotools.referencing.GeodeticCalculator;

/**
 *
 * @author Nick Julian
 */
public class Vessel {

    private double speed;
    private double heading;
    private Position position;

    private double maxSailingSpeed = 9;

    public static enum PointOfSail {
        IRONS,
        BEATING,
        BEAM_REACH,
        BROAD_REACH,
        RUN
    }

    @JsonCreator
    public Vessel(@JsonProperty("speed") double speed, @JsonProperty("heading") double heading, @JsonProperty("position") Position position) {
        setSpeed(speed);
        setHeading(heading);
        this.position = position;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        speed = MathUtil.ensureValueIsInRange(speed, 0, 50);
        this.speed = speed;
    }

    public double getHeading() {
        return heading;
    }

    public void setHeading(double heading) {
        this.heading = MathUtil.normalizeCompassHeading(heading);
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void move(Conditions conditions, int hours) {
        GeodeticCalculator calc = new GeodeticCalculator();
        calc.setStartingGeographicPoint(position.getDecimalLongitude(), position.getDecimalLatitude()); //Longitude, Latitude
        double leeway = calculateLeewayEffect(conditions);
        double actualHeading = MathUtil.normalizeCompassHeading(heading + leeway);
        double distanceToTravel = speed * hours;
        calc.setDirection(MathUtil.courseInDegreesToAzimuth(actualHeading) /* heading after leeway */, MathUtil.knotsToMeters(distanceToTravel) /* no DST calculation here because the method resolves one hour */);
        Point2D initialTrack = calc.getDestinationGeographicPoint();
        //Apply deviation from water current
        calc.setStartingGeographicPoint(initialTrack);
        distanceToTravel = conditions.getDrift() * hours;
        calc.setDirection(MathUtil.courseInDegreesToAzimuth(conditions.getSet()) /* Drift factor heading (i.e. 'set')*/, MathUtil.knotsToMeters(distanceToTravel) /* Drift factor speed */);
        Point2D trackMadeGood = calc.getDestinationGeographicPoint();
        this.position = new Position(trackMadeGood.getY(), trackMadeGood.getX());
    }

    private double calculateLeewayEffect(Conditions conditions) {
        //Figure out the wind's angle to the vessel's stern
        double headingDifference = MathUtil.getHeadingDifference(heading, conditions.getWindDirection());
        if ((headingDifference > 0) && (headingDifference < 90)) {
            //Leeway is falling on the starboard beam
            //It will push the boat's heading negative around the compass, so we
            //take the negative of the leeway value
            return (conditions.getLeeway() * -1);
        } else if ((headingDifference < 0) && (headingDifference > -90)) {
            //The wind is falling on the port beam.
            //It will push the boat's heading positive around the compass, so we 
            //take the regular value of the leeway
            return conditions.getLeeway();
        } else {
            //Wind is abaft the ship: no leeway applied
            return 0;
        }
    }

    public double getSailboatSpeed(Conditions conditions) {
        double windFactor = conditions.getWindSpeed() * 0.05;
        PointOfSail point = determineSailboatPoint(conditions);
        double maxSpeed = this.maxSailingSpeed;
        switch (point) {
            case IRONS:
                return 0;
            case BEATING:
                return MathUtil.roundDecimal(((maxSpeed * windFactor) * 0.25), 2);
            case BEAM_REACH:
                return MathUtil.roundDecimal(((maxSpeed * windFactor) * 0.80), 2);
            case BROAD_REACH:
                return MathUtil.roundDecimal((maxSpeed * windFactor), 2);
            case RUN:
                return MathUtil.roundDecimal(((maxSpeed * windFactor) * 0.70), 2);
        }
        return maxSpeed;
    }

    public PointOfSail determineSailboatPoint(Conditions conditions) {
        double windAngle = Math.abs(MathUtil.getHeadingDifference(this.heading, conditions.getWindDirection()));
        if (windAngle < 45) {
            return PointOfSail.IRONS;
        }
        if (windAngle > 45 && windAngle < 60) {
            return PointOfSail.BEATING;
        } else if (windAngle > 60 && windAngle < 90) {
            return PointOfSail.BEAM_REACH;
        } else if (windAngle > 90 && windAngle < 120) {
            return PointOfSail.BROAD_REACH;
        }
        return PointOfSail.RUN;
    }
}
