/*
 * Copyright (C) 2020 Nick Julian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.navsimulator.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import net.wizarddigital.navsimulator.model.DataPackage;

/**
 *
 * @author Nick Julian
 */
public class IOHandler {

    private IOHandler() {
        //Non-instantiable 'static' class
    }

    public static DataPackage loadData(String fileName) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        try {
            DataPackage de = mapper.readValue(Paths.get(fileName).toFile(), DataPackage.class);
            return mapper.readValue(Paths.get(fileName).toFile(), DataPackage.class);
        } catch (IOException e) {
            throw e;
        }

    }

    public static void saveData(DataPackage dataExport, String filePath) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
        if (!filePath.endsWith(".nvs")) {
          filePath += ".nvs";
        }
        try {
            writer.writeValue(new File(filePath), dataExport);
        } catch (IOException e) {
            throw e;
        }
    }

}
