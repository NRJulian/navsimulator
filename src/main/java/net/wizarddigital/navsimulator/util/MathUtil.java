/*
 * Copyright (C) 2020 Nick Julian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.navsimulator.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author Nick Julian
 */
public class MathUtil {

    public static final double METERS_PER_NAUTICAL_MILE = 1852;

    public static double getValueAlteration(double randomizationChance, double randomizationInterval) {
        //Coin flip to determine if the value should be altered...
        int random = (int) (Math.random() * 100);
        if (random <= randomizationChance) {
            int alterationAmount = (int) (Math.random() * randomizationInterval);
            //50% chance positive or negative
            random = (int) (Math.random() * 100);
            //Positive alteration
            if (random >= 50) {
                return alterationAmount;
            }
            //Negative alteration
            return alterationAmount * -1;
        }
        return 0;
    }

    private MathUtil() {
        //'Static' class
    }

    public static double knotsToMeters(double speedInKnots) {
        return speedInKnots * METERS_PER_NAUTICAL_MILE;
    }

    public static double normalizeCompassHeading(double heading) {
        while (heading < 0) {
            heading += 360;
        }
        while (heading > 360) {
            heading -= 360;
        }
        return heading;
    }

    public static double ensureValueIsInRange(double value, double min, double max) {
        return Math.min(Math.max(value, min), max);
    }

    //https://stackoverflow.com/a/8264007
    public static double convertDMSValuetoUnsignedDecimal(double degrees, double minutes, double seconds) {
        double decimal = ((minutes * 60) + seconds) / (60 * 60);
        decimal = degrees + decimal;
        return decimal;
    }

    //These two methods from:
    //https://stackoverflow.com/questions/3917340/geotools-how-to-do-dead-reckoning-and-course-calculations-using-geotools-class#3933331
    public static double courseInDegreesToAzimuth(double courseInDegrees) {
        Validate.isTrue(courseInDegrees >= 0.0 && courseInDegrees <= 360.0);
        double azimuth;
        if (courseInDegrees > 180.0) {
            azimuth = -180.0 + (courseInDegrees - 180.0);
        } else {
            azimuth = courseInDegrees;
        }
        return azimuth;
    }

    public static double azimuthToCourseInDegrees(double azimuth) {
        Validate.isTrue(azimuth >= -180.0 && azimuth <= 180.0);
        double courseInDegrees;
        if (azimuth < 0.0) {
            courseInDegrees = 360.0 + azimuth;
        } else {
            courseInDegrees = azimuth;
        }
        return courseInDegrees;
    }

    //https://stackoverflow.com/a/54820295
    /// The difference of two headings in degrees such that it is always in the range
    /// (-180, 180]. A negative number indicates [h2] is to the left of [h1].
    public static double getHeadingDifference(double firstHeading, double secondHeading) {
        double left = firstHeading - secondHeading;
        double right = secondHeading - firstHeading;
        if (left < 0) {
            left += 360;
        }
        if (right < 0) {
            right += 360;
        }
        return left < right ? -left : right;
    }

    //https://stackoverflow.com/questions/2808535/round-a-double-to-2-decimal-places#2808648
    public static double roundDecimal(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bigDecimal = BigDecimal.valueOf(value);
        bigDecimal = bigDecimal.setScale(places, RoundingMode.HALF_UP);
        return bigDecimal.doubleValue();
    }

}
