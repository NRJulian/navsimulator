# NavSimulator
NavSimulator is a desktop app for simulating the motion of a vessel on water in latitude and longitude. 

![Screenshot](./images/screenshot.png)

NavSimulator is intended to be used as a training aid for those learning traditional chart based navigation skills including dead reckoning, drift and leeway. You can set custom tide and wind conditions or randomly generate them, with the option to hide the generated values to allow for figuring from unknowns. 

You can set a course with an expected destination, process the course in the app, and compare the results of your expected destination with where the app actually placed the vessel. Then you can figure your set and drift, plot the next leg of the course, and process it again.   

## Installation

NavSimulator is a portable Java desktop application and can be run by simply downloading and double-clicking the most recent JAR file available on the releases page. You will need to have a Java runtime installed on your machine. You can download AdoptOpenJDK [here](https://adoptopenjdk.net/).


## Guide:

The NavSimulator UI is broken into 4 distinct panels.
![Screenshot](./images/tutorial_1.png)
1. The 'Vessel' panel
2. The 'Conditions' panel
3. The 'Randomization' panel
4. The 'Log' panel

### The 'Vessel' Panel:
On this panel you can see and edit the following values:
- Your vessel's latitude and longitude
- Your vessel's speed
- Your vessel's heading

You can also toggle 'Sailing Mode' on and off. In 'Sailing Mode', your vessel's speed is determined by the wind speed and heading (tack).

### The 'Conditions' Panel
On this panel, you can see and edit the following environmental values:
- Set
- Drift
- Wind Direction
- Wind Speed
- Leeway

You can also set the toggle for linking the ship's leeway to wind speed.

### The 'Randomization' Panel
From this panel you can apply random alterations to the conditions. Every time you click "Apply", the program will process random alterations for every checked option. The '% Chance' value is first checked to see if an alteration will be made at all, and then if so the variance applied will be a random number +/- the possible variance value.

### The 'Log' Panel
The Log will record hour vessel's location and the wind condition after every voyage step

Finally at the bottom of the page is a dropdown to set how many hours to process and the 'Process' button which calculates and sets your vessel's new position after a set amount of time.

## Example Usage:


NavSimulator is intended as a training aid. It should be used with a chart when studying nautical navigation. An example usage of NavSimulator would be as follows:

1. Mark the starting location of your 'vessel' on your physical chart.
2. In NavSimulator, set the Latitude/Longitude location of your vessel on the 'Vessel' panel. Also put in your vessel's heading and speed.
3. On your physical chart, use dead reckoning to figure your vessel's position in an hour and mark it.
3. In NavSimulator, use the 'Hide' checkboxes on the Conditions panel to hide the set and drift numbers.
4. Use the 'Randomization' panel to randomly alter the (now hidden) set and drift values. Do this by setting the '% Chance' for 'Alter set' and 'Alter drift' to 100% and clicking the 'Apply' button several times. Raising the 'Possible variance' value will cause the set and drift to be more radically affected by the randomization.
5. Make sure the time selector in the bottom right is set to 1 hour, and click 'Process'. NavSimulator will factor in set/drift/leeway and change your vessel's location on the 'Vessel' panel to the factored spot. It will also record the details of your journey in the log section. Mark the new location that NavSimulator calculated on your physical chart, alongside your DR position.
6. The location you marked for your dead reckoning and the location NavSimulator put your vessel in should be different. This, of course, is because NavSimulator factored in the set and drift. Now, you can use the difference between the two to [calculate](https://www.youtube.com/watch?v=LZ4TQ3sZQak) what the set and drift are. You should then be able to adjust your heading with a proper course to steer and your next hour should put your dead reckoning and NavSimulator's position in the same spot.

## Licensing:

NavSimulator is written entirely in Java Swing and is free (libre) software licensed under the GNU GPL v.3.0 license. NavSimulator uses the following open-source libraries:
 - [GeoTools](https://github.com/geotools/geotools) ([LGPL v2.1](https://github.com/geotools/geotools/blob/main/LICENSE.md))
 - [Apache Commons Lang](https://github.com/apache/commons-lang) ([Apache 2.0](https://github.com/apache/commons-lang/blob/master/LICENSE.txt))
 - [Flatlaf](https://github.com/JFormDesigner/FlatLaf) ([Apache 2.0](https://github.com/JFormDesigner/FlatLaf/blob/main/LICENSE))
 - [Jackson Databind](https://github.com/FasterXML/jackson-databind) ([Apache 2.0](https://github.com/FasterXML/jackson-databind/blob/master/LICENSE))

NavSimulator is written using NetBeans and Maven.
 - [Using NetBeans and Maven](http://wiki.netbeans.org/Maven)